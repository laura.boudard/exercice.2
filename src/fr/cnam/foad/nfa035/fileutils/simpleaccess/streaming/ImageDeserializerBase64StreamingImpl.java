package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer <AbstractImageFrameMedia> {

    private OutputStream sourceOut;

    public ImageDeserializerBase64StreamingImpl(OutputStream sourceOut) {
        this.sourceOut = sourceOut;
    }

    @Override
    public void deserialize(Object media) throws IOException {

    }

    @Override
    public OutputStream getSourceOutputStream() {

        return sourceOut;
    }

    @Override
    public <K extends InputStream> K getDeserializingStream(AbstractImageFrameMedia media) throws IOException {
        return null;
    }
}
