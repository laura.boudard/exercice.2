package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageSerializerBase64StreamingImpl implements ImageStreamingSerializer {

    private InputStream sourceInputStream;

    @Override
    public void serialize (Object S, Object M) throws IOException {

    }

    @Override
    public OutputStream getSerializingStream (Object M) throws IOException {

        return M;
    }

    @Override
    public InputStream getSourceInputStream (Object S) throws IOException {

        return S;
    }
}
