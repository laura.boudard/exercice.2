package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.*;


public class ImageByteArrayFrame extends AbstractImageFrameMedia {


    public ImageByteArrayFrame(ByteArrayOutputStream byteArrayOutputStream) {
    }

    /**
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getEncodedImageOutput() throws IOException{
       return (OutputStream) getChannel();
    }

    /**
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return null;
    }


}
